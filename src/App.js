import { useState } from 'react';


// todo : Moving Components Into Separate Files

export default function App() {
	const [ items, setItems ] = useState([]);

	function handelItems(item) {
		setItems((items) => [ ...items, item ]);
	}

	function handleDeleteItem(id) {
		setItems((items) => items.filter((item) => item.id != id));
	}

	function handleUpdateItem(id) {
		setItems((items) => items.map((item) => (item.id === id ? { ...item, packed: !item.packed } : item)));
  }
  
  function handleCleanList ()
  {
    const confirmed = window.confirm( 'Are you sure to delete all ' )
    
    if ( confirmed )
    {
      setItems([])
    }
    
  }

	return (
		<div className="app">
			<Logo />
			<Form onAdditem={handelItems} />
			<PackingList items={items} onDeleteItem={handleDeleteItem} onUpdateItem={handleUpdateItem} onCleanList={handleCleanList} />
			<Stats items={items} />
		</div>
	);
}

function Logo() {
	return <h1>Far away </h1>;
}
function Form({ onAdditem }) {
	const [ discription, setDiscription ] = useState('');
	const [ quntity, setQuntity ] = useState(1);

	function handleSubmit(e) {
		e.preventDefault();

		const newItem = { discription, quntity, packed: false, id: Date.now() };

		console.log('new record ', newItem);

		onAdditem(newItem);

		setDiscription('');
		setQuntity(1);
	}

	return (
		<form className="add-form" onSubmit={handleSubmit}>
			<h3>what you need for trip ?</h3>
			<select value={quntity} onChange={(e) => setQuntity(e.target.value)}>
				{Array.from({ length: 20 }, (_, i) => i + 1).map((num) => (
					<option value={num} key={num}>
						{num}
					</option>
				))}
				{/* <option value={1}>1</option>
        <option value={2}>2</option>
        <option value={3}>3</option> */}
			</select>
			<input
				type="text"
				placeholder="item..."
				value={discription}
				onChange={(e) => setDiscription(e.target.value)}
			/>

			<button>Add</button>
		</form>
	);
}
function PackingList({ items, onDeleteItem, onUpdateItem,onCleanList }) {
	const [ sortBy, setSortBy ] = useState('input');

	let sortedItems;

	if (sortBy === 'input') sortedItems = items;

	if (sortBy === 'discription') sortedItems=items.slice().sort((a, b) => a.discription.localeCompare(b.discription));
	if (sortBy === 'packed') sortedItems=items.slice().sort((a, b) => Number(a.packed)- Number(b.packed));

	return (
		<div className="list">
			<ul>
				{sortedItems.map((item) => (
					<Item item={item} onDeleteItem={onDeleteItem} onUpdateItem={onUpdateItem} key={item.id} />
				))}
			</ul>

			<div className="action">
				<select value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
					<option value="input"> Sort by Pack</option>
					<option value="discription"> Sort by Discription</option>
					<option value="packed"> Sort by Pack</option>
        </select>
        <button onClick={onCleanList}>Clean List</button>
			</div>
		</div>
	);
}

function Item({ item, onDeleteItem, onUpdateItem }) {
	return (
		<li>
			<input type="checkbox" value={item.packed} onChange={() => onUpdateItem(item.id)} />
			<span style={item.packed ? { textDecoration: 'line-through' } : {}}>
				{item.quntity} {item.discription}
			</span>{' '}
			<button onClick={() => onDeleteItem(item.id)}>❌</button>{' '}
		</li>
	);
}

function Stats({ items }) {
	if (!items.length) return <p className="stats">start adding items </p>;
	const numItems = items.length;

	const numPack = items.filter((item) => item.packed).length;
	const percentage = Math.round(numPack / numItems * 100);

	return (
		<footer className="stats">
			{percentage === 100 ? (
				'you are ready to go '
			) : (
				`you have ${numItems} items in our list. and already packed ${numPack} (${percentage})`
			)}
		</footer>
	);
}
